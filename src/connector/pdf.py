from pdfreader import PDFDocument

from bs4 import BeautifulSoup

import sys

class PdfMetadata:
    def __init__ ( self, title=None, confName=None, doi=None ):
        self.title = title
        self.confName = confName
        self.doi = doi

    def __str__ ( self ):
        return "# %s\nDOI: %s\nConference: %s\n"%(self.title, self.doi, self.confName)

def xmpMetadataAttr ( tag, attr ):
    for a in tag.attrs:
        if attr.lower() in a.lower():
            return tag.attrs[a].strip()
    return None

def xmpMetadata ( doc ):
    xmp = BeautifulSoup ( doc.root.Metadata.stream, "xml" )
    descr = xmp.find_all ( "Description" )

    title = None
    confName = None
    doi = None
    for d in descr:
        doi = xmpMetadataAttr( d, "doi" )
        title = xmpMetadataAttr ( d, "title" )
        confName = xmpMetadataAttr ( d, "publicationName" )

    titleTag = xmp.find_all ( "title" )
    for tt in titleTag:
        title = tt.getText().strip()

    return PdfMetadata ( title, confName, doi )

def discoverMetadata ( path ):
    fd = open(path, "rb")
    doc = PDFDocument(fd)
    xmp = xmpMetadata ( doc )

    if (doc.metadata["Title"]) > xmp.title:
        xmp.title = doc.metadata["Title"]

    return xmp



#def main ():
#    arg = sys.argv[1]
#    print ( discoverMetadata ( arg ) )
#
#main ()
