import os.path

from . import pdf
from . import doi
from . import usenix
from . import zotero
from . import cr

class Extractor:
    @staticmethod
    def isSupported ( item ):
        raise NotImplemented
    @staticmethod
    def extract ( item, **kwargs ):
        raise NotImplemented
        pass

class CannotHandleError ( Exception ):
    pass

class GenericExtractor ( Extractor ):
    @staticmethod
    def isSupported ( item ):
        return False

    @staticmethod
    def extract ( item ):
        pass


class UsenixExtractor ( Extractor ):
    @staticmethod
    def isSupported ( item ):
        return usenix.UsenixMeta.isSupported ( item )
    @staticmethod
    def extract ( item, **kwargs ):
        print ("USENIX EXTRACT")
        return usenix.UsenixMeta.Extract ( item, **kwargs )


class DoiExtractor ( Extractor ):
    @staticmethod
    def isSupported ( item ):
        if len(item.input) > 0 or len(item.url) > 0 or len(item.meta.doi) > 0:
            return True
        else:
            return False

    @staticmethod
    def extract ( item, **kwargs ):
        inputs = []
        if len(item.meta.doi) > 0:
            inputs += [ item.meta.doi ]
        if len(item.url) > 0:
            inputs += [ item.url ]
        inputs += [ item.input ]

        dois = []
        for inputEntry in inputs:
            try:
                dois = doi.guessDoi ( inputEntry )
            except (doi.NoDoiError, doi.ContentParsingError):
                continue

        if len(dois) == 1:
            item.meta.doi = dois[0][0]
        elif len(dois) > 1:
            item.meta.doi = dois[0][0]
        else:
            raise CannotHandleError

        return CrossrefExtractor().extract ( item, **kwargs )


class CrossrefExtractor ( Extractor ):
    @staticmethod
    def isSupported ( item ):
        try:
            if len(item.url) > 0 or len(item.meta.doi) > 0 or len(item.zotero["title"]) > 0:
                return True
        except KeyError:
            if len(item.url) > 0 or len(item.meta.doi) > 0:
                return True
        return False

    @staticmethod
    def extract( item, **kwargs ):
        cr = kwargs["crossref"]
        doi = item.meta.doi
        title = ""
        authors = ""
        if item.zotero is not None:
            if "title" in item.zotero:
                title = item.zotero["title"]
            if "creators" in item.zotero:
                authors = item.zotero["creators"]

        results = cr.query ( doi=doi, title=title, authors=authors )
        if len(results) == 1:
            (item.zotero,item.meta.pdf, _) = results[0]
            return "finished"
        elif len(results) > 1 :
            for (zot, pdfs, w) in results:
                if w["DOI"] == doi or (type(title) is str and title.startswith(w["title"][0])):
                    item.zotero = zot
                    item.meta.pdf = pdfs
                    return "finished"

        raise CannotHandleError()


class PdfExtractor ( Extractor ):
    @staticmethod
    def isSupported ( item ):
        url = item.input

        if os.path.isfile ( url ):
            item.meta.pdf = url
            return True
        else:
            item.meta.pdf = None
            return False

    @staticmethod
    def extract ( item, **kwargs ):
        if item.meta.pdf == None:
            raise TypeError
        item.pdfreader = discoverMetadata ( item.meta.pdf )
        if item.pdfreader.doi == None:
            if item.pdfreader.title != None:
                # FIXME
                raise NotImplemented
            else:
                raise CannotHandleError()

        if item.pdfreader.doi != None:
            return DoiExtractor().extract ( item )
        if item.pdfreader.title != None:
            return CrossrefExtractor().extract ( item )

        raise CannotHandleError()


class Item:
    class Meta:
        def __init__ ( self ):
            self.pdf = ""
            self.doi = ""
    def __init__ ( self, url ):
        self.input = url
        self.meta = Item.Meta()
        self.zotero = None
        self.crossref = None
        self.pdfreader = None
        self.url = ""

    @staticmethod
    def characterize ( url, **kwargs ):
        item = Item ( url )

        redo = False
        while True:
            for e in EXTRACTORS:
                try:
                    if e.isSupported ( item ):
                        redo = True
                        cmd = e.extract ( item, **kwargs )
                        if cmd == "finished":
                            return item
                except (CannotHandleError, usenix.UsenixNotUsenix):
                    pass
            if redo == False:
                raise CannotHandleError
        return item

class TestInserter:
    def __init__ ( self, zot=None, cr=None, **kwargs ):
        self.zot = zot
        self.cr = cr
        self.sh = None
        pass

    def Insert ( self, item ):
        pass

class ZoteroInserter:
    def __init__ ( self, zot, cr=None, sh=None, **kwargs ):
        self.zot = zot
        self.sh = sh
        self.cr = cr
        self.zotero_collection = None
        self.collection = kwargs["collection"]

    def findCollection ( self ):
        if len(self.collection) <= 0:
            return

        col = self.zot.getCollection ( self.collection )
        if col == None:
            col = self.zot.createCollection ( self.collection )

        self.zotero_collection = col;

    def Insert ( self, item ):
        if self.zotero_collection == None:
            self.findCollection ()

        zotItem = None
        if item.zotero != None:
            zotItem = self.zot.createItem ( item.zotero )
            if self.zotero_collection != None:
                self.zotero_collection.addTo ( zotItem )
        else:
            return False

        pdflinks = []
        attached = False
        if type(item.meta.pdf) is str and len(item.meta.pdf) > 0:
            if os.path.isfile ( item.meta.pdf ):
                zotItem.addAttachment ( item.meta.pdf )
                attached = True
            else:
                pdflinks = [ item.meta.pdf ]
        elif type(item.meta.pdf) is list:
            pdflinks = item.meta.pdf

        if len(pdflinks) > 0 and not attached:
            try:
                zotItem.transferPDF ( item.meta.pdf )
            except zotero.DownloadError:
                if self.sh != None:
                    zotItem.transferEvilPDF ( self.sh, item.meta.doi )
        else:
            if self.sh != None:
                zotItem.transferEvilPDF ( self.sh, item.meta.doi )


def StartExtraction ( url, inserter, **kwargs ):
    item = Item.characterize ( url, zotero=inserter.zot, scihub=inserter.sh, crossref=inserter.cr, **kwargs )
    if item != None:
        inserter.Insert ( item )
    return item


EXTRACTORS = [
    PdfExtractor,
    DoiExtractor,
    UsenixExtractor,
    GenericExtractor
]
