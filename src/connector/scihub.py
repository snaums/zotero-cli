# -*- coding: utf-8 -*-

"""
Sci-API Unofficial API
[Search|Download] research papers from [scholar.google.com|sci-hub.io].

@author zaytoun
from https://github.com/zaytoun/scihub.py
"""

import re
import logging

import requests
import urllib3
from bs4 import BeautifulSoup

# constants
HEADERS = {'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64; rv:27.0) Gecko/20100101 Firefox/27.0'}
SH_LINK_SITE = 'https://sci-hub.now.sh/'

class SciHub(object):
    """
    SciHub class can search for papers on Google Scholars 
    and fetch/download papers from sci-hub.io
    """

    def __init__(self, logger=None, baseurl = ""):
        self.sess = requests.Session()
        self.sess.headers = HEADERS
        self.baseurl = baseurl

        if logger == None:
            logger = logging.getLogger ( "sci-hub" )
        self.logger = logger

        self.available_base_url_list = []
        self.base_url = ""

    def _get_available_scihub_urls(self):
        '''
        Finds available scihub urls via https://sci-hub.now.sh/
        '''
        self.logger.debug("_get_available_scihub_urls")
        urls = []
        res = requests.get(SH_LINK_SITE)
        s = BeautifulSoup(res.content, "html.parser")

        for a in s.find_all('a', href=True):
            if 'sci-hub.' in a['href']:
                urls.append(a['href'])
        return urls

    def set_proxy(self, proxy):
        '''
        set proxy for session
        :param proxy_dict:
        :return:
        '''
        self.logger.debug("set_proxy")
        if proxy:
            self.sess.proxies = {
                "http": proxy,
                "https": proxy, }

    def _change_base_url(self):
        self.logger.debug("_change_base_url")
        if not self.available_base_url_list:
            raise OutOfUrlsException('Ran out of valid sci-hub urls')
        del self.available_base_url_list[0]
        self.base_url = self.available_base_url_list[0] + '/'
        self.logger.info("I'm changing to {}".format(self.available_base_url_list[0]))

    def fetch(self, identifier):
        """
        Fetches the paper by first retrieving the direct link to the pdf.
        If the indentifier is a DOI, PMID, or URL pay-wall, then use Sci-Hub
        to access and download paper. Otherwise, just download paper directly.
        """
        self.logger.debug("fetch(%s)", identifier)

        if self.base_url == "" :
            if len(self.baseurl) > 0:
                self.avaible_base_url_list = [ self.baseurl ]
            else:
                self.available_base_url_list  = []
            self.available_base_url_list += self._get_available_scihub_urls()
            self.base_url = self.available_base_url_list[0] + ("" if self.available_base_url_list[0].endswith("/") else "/")

        try:
            url = self._get_direct_url(identifier)

            # verify=False is dangerous but sci-hub.io 
            # requires intermediate certificates to verify
            # and requests doesn't know how to download them.
            # as a hacky fix, you can add them to your store
            # and verifying would work. will fix this later.
            res = self.sess.get(url, verify=False)

            if res.headers['Content-Type'] != 'application/pdf':
                self._change_base_url()
                self.logger.info('Failed to fetch pdf with identifier %s '
                                           '(resolved url %s) due to captcha' % (identifier, url))
                raise CaptchaNeedException('Failed to fetch pdf with identifier %s '
                                           '(resolved url %s) due to captcha' % (identifier, url))
                # return {
                #     'err': 'Failed to fetch pdf with identifier %s (resolved url %s) due to captcha'
                #            % (identifier, url)
                # }
            else:
                return {
                    'pdf': res.content,
                    'url': url,
                    'name': self._generate_name(res)
                }

        except requests.exceptions.ConnectionError:
            self.logger.info('Cannot access {}, changing url'.format(self.available_base_url_list[0]))
            self._change_base_url()
            raise ConnectionError()
        except requests.exceptions.RequestException as e:
            self.logger.debug('Failed to fetch pdf with identifier %s (resolved url %s) due to request exception.'
                       % (identifier, url))
            raise RequestException (
                'Failed to fetch pdf with identifier %s (resolved url %s) due to request exception. %s'
                       % (identifier, url, e) )

    def _get_direct_url(self, identifier):
        """
        Finds the direct source url for a given identifier.
        """
        self.logger.debug("_get_direct_url(%s)", identifier)
        id_type = self._classify(identifier)

        return identifier if id_type == 'url-direct' \
            else self._search_direct_url(identifier)

    def _search_direct_url(self, identifier):
        """
        Sci-Hub embeds papers in an iframe. This function finds the actual
        source url which looks something like https://moscow.sci-hub.io/.../....pdf.
        """
        self.logger.debug ( "_search_direct_url(%s)", identifier )

        res = self.sess.get(self.base_url + identifier, verify=False)
        s = BeautifulSoup(res.content, "html.parser")
        iframe = s.find_all('iframe')

        for e in iframe:
            if e.get("id") != "pdf":
                continue
            return e.get('src') if not e.get('src').startswith('//') \
                    else 'http:' + e.get('src')
        raise HTMLParsingException()

    def _classify(self, identifier):
        """
        Classify the type of identifier:
        url-direct - openly accessible paper
        url-non-direct - pay-walled paper
        pmid - PubMed ID
        doi - digital object identifier
        """
        self.logger.debug("_classify (%s)", identifier )

        if (identifier.startswith('http') or identifier.startswith('https')):
            if identifier.endswith('pdf'):
                return 'url-direct'
            else:
                return 'url-non-direct'
        elif identifier.isdigit():
            return 'pmid'
        else:
            return 'doi'

    def _generate_name(self, res):
        """
        Generate unique filename for paper. Returns a name by calcuating 
        md5 hash of file contents, then appending the last 20 characters
        of the url which typically provides a good paper identifier.
        """
        self.logger.debug("generate_name(%s)", res.url)

        name = res.url.split('/')[-1]
        name = re.sub('#view=(.+)', '', name)
        return name[-40:]

class CaptchaNeedException(Exception):
    pass
class ConnectionError(Exception):
    pass
class OutOfUrlsException(Exception):
    pass
class RequestException(Exception):
    pass
class HTMLParsingException(Exception):
    pass
