from crossref.restful import Works, Etiquette

from .zotero import *
from datetime import date

import logging

import re
import json

def urlize ( txt ):
    return re.sub( r'[^a-z0-9\-]', '', txt.lower().replace(" ", "_" ) )

def authorsTransfer ( tmpl, w ):
    tmpl["creators"] = []
    for a in w["author"]:
        creat = {
            "creatorType":"author",
            "firstName": a["given"],
            "lastName": a["family"]
        }
        tmpl["creators"] += [ creat ]
    return True

def accessDateTransfer ( tmpl, w ):
    tmpl["accessDate"] = date.today().isoformat()
    return True

def dateTransfer ( tmpl, w ):
    d = w["issued"]["date-parts"][0]
    try:
        dt = date ( d[0], d[1], d[2] )
    except:
        try:
            d = w["deposited"]["date-parts"][0]
            dt = date ( d[0], d[1], d[2] )
        except:
            return False

    tmpl["date"] = dt.isoformat()
    return True

def titleTransfer ( tmpl, w ):
    try:
        t = "%s: %s"%(w["title"][0], w["subtitle"][0])
    except:
        t = w["title"][0]

    tmpl["title"] = t
    tmpl["shortTitle"] = w["title"][0]
    return True

##
# itemTypes
# * crossref type-name to zotero item type
#
# transferRules.
# * associates zotero-type names with rules on how to convert crossref to zotero.
# * on the left side is the zotero-dict-name, on the right side either
#   * a string, then its the name (or path) to the data in crossref-format
#   * a function, then the function will be called, giving it the whole objects
# * the path-strings can descent into the data structures with '/', e.g. a/b -> tmpl["a"]["b"]
# * if an entry is a list, only the zero (0) element will be used
# 
# * some data is easy to copy, then just use the string and copy the data. Other things are harder to copy, like dates, author names, etc. Then use a function, which does transfer data from crossref to zotero dict.
#
class MyCrossRef ():
    '''Representation of the crossref API for transferring data between crossref data to zotero dict'''
    itemTypes = {
        "paper-conference": "conferencePaper",      ## default atm
    }
    itemTypeDefault = "conferencePaper"
    transferRules = {
        "conferencePaper": {
            "title": titleTransfer,
            "creators": authorsTransfer,
            "DOI": "DOI",
            "ISBN": "ISBN",
            "accessDate": accessDateTransfer,
            "date": dateTransfer,
            "proceedingsTitle": "container-title",
            "conferenceName": "event/name",
            "place": "event/location",
            "url": "link/URL",
            "publisher": "publisher",
            "shortTitle": "short-title",
        }
    }
    def __init__ ( self, myzot, prname=ProgramName, prversion=ProgramVersion, prurl=ProgramUrl, premail=ProgramEmail, logger=None ):
        self.etiquette = Etiquette ( prname, prversion, prurl, premail )
        self.works = Works ( etiquette=self.etiquette )
        self.myzot = myzot
        self.logger = logger
        if self.logger == None:
            self.logger = logging.getLogger("MyCrossref")

    def queryDOI ( self, doi ):
        return self.works.doi ( doi )

    def queryTitle ( self, title ):
        return self.works.query ( title )

    def itemType ( self, works ):
        if works["type"] in self.itemTypes:
            return self.itemTypes[works["type"]]
        else:
            return self.itemTypeDefault

    def itemResolve ( self, tmpl, w, left, right ):
        l = left.split("/")
        r = right.split("/")

        value = ""
        wx = w
        try:
            for ri in r:
                if type(wx) is list:
                    try:
                        wx = wx[0]
                    except:
                        wx = {}
                wx = wx[ri]
        except KeyError:
            self.logger.warning("Key error on %s", right )
            return False

        if type(wx) is str:
            value = wx
        elif type(wx) is list:
            try:
                value = wx[0]
            except:
                value = ""
        else:
            self.logger.warning("traversing the work has not worked out, w[%s] -> %s", right, wx)
            return False

        target = tmpl
        for li in l[:-1]:
            if type(target) is list:
                target = target[0]
            target = target[li]

        target[l[-1]] = value
        return True

    def itemResolveCall ( self, tmpl, w, left, fncall ):
        return fncall ( tmpl, w )

    def itemDoTransfer ( self, tmpl, w, rules ):
        ret = None

        level = self.logger.level;
        if level <= logging.DEBUG:
            with open("crossref-output", "a+") as f:
                f.write ( str(w) )

        exc = None
        for f in rules:
            try:
                if callable(rules[f]):
                    ret = self.itemResolveCall ( tmpl, w, f, rules[f] )
                else: ## type(rules[f]) is str
                    ret = self.itemResolve ( tmpl, w, f, rules[f] )
                if ret != True:
                    self.logger.warning ( "a problem occured when transferring %s", f )
            except KeyError:
                self.logger.warning ( "Key error")
                exc = QueryParseError("Key Error")

        if level <= logging.DEBUG:
            with open( "crossref-" +urlize(tmpl["title"])+".json", "w+" ) as f:
                f.write ( json.dumps(w))
            with open( "zotero-" +urlize(tmpl["title"])+".json", "w+" ) as f:
                f.write ( json.dumps(tmpl) )

        if exc != None:
            raise exc

        return True

    def itemTransfer ( self, tmpl, w ):
        if tmpl["itemType"] in self.transferRules:
            return self.itemDoTransfer ( tmpl, w, self.transferRules[tmpl["itemType"]] )
        else:
            return self.itemDoTransfer ( tmpl, w, self.transferRules[self.itemTypeDefault] )

    def getPdfUrls ( self, tmpl, w ):
        result = []
        if len(w["link"]) > 0:
            for link in w["link"]:
                # prefer PDFs over unspecified URLs
                if link["content-type"] == "application/pdf":
                    result = [ link["URL"] ] + result
                else:
                    result += [ link["URL"] ]

        return result

    def query ( self, doi=None, title=None, authors=None ):
        if doi != None:
            works = self.queryDOI ( doi )
        elif title != None:
            works = self.queryTitle ( title )

        if works is None:
            return []

        if type(works) is dict:
            works = [works]

        result = []
        for w in works:
            itemType = self.itemType ( w )
            tmpl = self.myzot.itemTemplate ( itemType )
            ret = self.itemTransfer ( tmpl, w )
            if ret == False:
                return None

            urls = self.getPdfUrls ( tmpl, w )

            result += [ (tmpl, urls, w) ]

        return result
