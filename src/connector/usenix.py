from bs4 import BeautifulSoup
import re
import requests
from datetime import datetime

from . import zotero

class UsenixNotUsenix( Exception ):
    pass

class UsenixMeta:
    url = re.compile ( "usenix.org", re.IGNORECASE )
    def __init__ ( self ):
        self.author = ""
        self.title = ""
        self.publication_date = ""
        self.conference_title = ""
        self.firstpage = ""
        self.lastpage = ""
        self.isbn = ""
        self.pdf_url = ""

    def __str__ ( self ):
        result = "Usenix:\n"
        for d in dir(self):
            x = getattr(self, d)
            if type(x) is str:
                result += (d + " => " + x + "\n")

        return result

    @staticmethod
    def Decode ( url ):
        # raises exception if not usenix
        UsenixMeta.Probe ( url )

        resp = requests.get ( url )
        s = BeautifulSoup( resp.content, "html.parser")
        meta = s.find_all("meta")
        result = UsenixMeta()
        metaname = "citation_"
        metalen = len(metaname)

        # scratch the metadata from HTML meta-tags
        for tag in meta:
            try:
                name = tag["name"]
                content = tag["content"].strip().replace("{", "").replace("}", "")
                if not name.startswith ( metaname ):
                    continue
            except KeyError:
                continue

            attrname = name[metalen:]
            newAttr = ""
            if hasattr(result, attrname) and len(getattr(result, attrname)):
                old = getattr(result, attrname)
                newAttr = old + "," + content
            else:
                newAttr = content
            setattr ( result, attrname, newAttr )

        # find PDF
        a = s.find_all("a")
        for ai in a:
            try:
                if ai["href"].endswith(".pdf"):
                    result.pdf_url = ai["href"]
                    break
            except KeyError:
                pass

        return result

    @staticmethod
    def isSupported ( item ):
        try:
            return UsenixMeta.Probe ( item.url )
        except UsenixNotUsenix:
            return UsenixMeta.Probe ( item.input )

    @staticmethod
    def Probe ( url ):
        res = UsenixMeta.url.search ( url )
        if type(res) is re.Match:
            return True
        else:
            raise UsenixNotUsenix()

    @staticmethod
    def ZoteroAuthor ( firstname, lastname ):
        return [ { "creatorType": "author", "firstName": firstname, "lastName": lastname} ]


    @staticmethod
    def Authors ( authors ):
        a = authors.split(",")
        result = []
        for ai in a:
            nameparts = ai.split(" ")
            if len(nameparts) == 1:
                result += UsenixMeta.ZoteroAuthor ( nameparts[0], "" )
            else:
                firstname = ""

                for xi in nameparts[:-1]:
                    firstname += xi + " "
                firstname = firstname[:-1]
                result += UsenixMeta.ZoteroAuthor ( firstname, nameparts[-1] )
        return result

    @staticmethod
    def Extract ( item, **kwargs ):
        usenix = None
        url = item.url
        if len(url) == 0:
            url = item.input

        try:
            usenix = UsenixMeta.Decode ( url )
        except UsenixNotUsenix:
            raise UsenixNotUsenix

        if "zotero" in kwargs and kwargs["zotero"] != None:
            zot = kwargs["zotero"]
            zotero = zot.itemTemplate ( "conferencePaper" )
        elif "template" in kwargs:
            zotero = kwargs["template"]

        zotero["title"] = usenix.title
        zotero["creators"] = UsenixMeta.Authors ( usenix.author )
        zotero["isbn"] = usenix.isbn
        zotero["accessDate"] = "CURRENT_TIMESTAMP"
        zotero["date"] = usenix.publication_date
        zotero["conferenceName"] = zotero["proceedingsTitle"] = usenix.conference_title
        zotero["url"] = item.url
        zotero["publisher"] = "Usenix"
        zotero["extra"] = usenix.firstpage + " " + usenix.lastpage
        item.zotero = zotero
        item.meta.pdf = [usenix.pdf_url]
        return "finished"
