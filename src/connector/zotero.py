from pyzotero import zotero

ProgramName = "Zotero-CLI"
ProgramVersion = "0.1"
ProgramUrl = "https://codeberg.org/snaums/zotero-cli"
ProgramEmail = "me@stefannaumann.de"

from .cr import MyCrossRef
import requests

import re
import os
import os.path

def urlize ( txt ):
    return re.sub( r'[^a-z0-9z-]', '', txt.lower().replace(" ", "-" ) )

class MyCollection:
    def __init__ ( self, zot, key ):
        self.key = key
        self.zot = zot

    def items ( self ):
        return self.zot.collection_items ( self.key )

    def present ( self, url ):
        items = self.zot.collection_items ( self.key )
        for i in items:
            try:
                if i["data"]["url"] == url or i["data"]["extra"] == url:
                    return True
            except:
                pass
        return False

    def addTo ( self, item ):
        if type(item) is str:
            dic = self.zot.item(item)
            item = MyItem ( zot, dic )

        if type(item) is MyItem:
            return self.zot.addto_collection ( self.key, item.dic )
        else:
            raise Exception

class MyError(Exception):
    def __init__ ( self, txt):
        self.err = txt
class DownloadError(MyError):
    pass
class QueryError(MyError):
    pass
class QueryParseError(MyError):
    pass
class AlreadyPresentError(MyError):
    pass

class MyItem:
    path = "pdfs"
    def __init__ ( self, zot, dic ):
        self.dic = dic
        self.zot = zot

    def addAttachment ( self, path ):
        return self.zot.attachment_simple ( path, self.dic["key"] )

    def downloadPDF ( self, url, target ):
        os.makedirs ( os.path.dirname(target), exist_ok=True );

        r = requests.get(url)
        ctype = r.headers.get("content-type")
        ctype = ctype.split(";")
        ct = []
        for c in ctype:
            ct += [ c.strip() ]
        if r.status_code != requests.codes.ok or ("application/pdf" not in ct):
            print ("ERROR Downloading file: ", url, " code: ", r.status_code )
            raise DownloadError("[%d], content-type: %s"%(r.status_code, ct))

        with open(target, "wb") as f:
            f.write ( r.content )

        return target

    def downloadPath ( self, path ):
        target = ""
        try:
            target = os.path.join ( path, 
                    urlize(self.dic["data"]["conferenceName"][:10])+"-"+
                    urlize(self.dic["data"]["title"][:15]) + ".pdf"
            )
        except KeyError:
            target = os.path.join ( path, self.dic["key"]+".pdf" )
        return os.path.join ( os.getcwd(), target )

    def transferEvilPDF ( self, sh, url ):
        if sh == None:
            return False
        target = self.downloadPath ( self.path )
        os.makedirs ( os.path.dirname(target), exist_ok=True );

        res = sh.fetch ( url )
        if "err" in res:
            raise DownloadError ( res["err"] )

        with open(target, "wb") as f:
            f.write ( res["pdf"] )

        self.addAttachment( [target] )


    def transferPDF ( self, urls ):
        already = []
        for u in urls:
            if u in already:
                continue
            already += [ u ]

            target = self.downloadPath ( self.path )
            target = self.downloadPDF ( u, target )

            if target != False:
                self.addAttachment ( [target] )

class MyZotero:
    def __init__ ( self, libid, libtype, apikey, logger, pdfpath="pdfs" ):
        self.zot = zotero.Zotero ( libid, libtype, apikey )
        self.logger = logger
        self.logger.info ( self.zot.key_info() )
        MyItem.path = pdfpath

    def getCollection ( self, folder, parent=None ):
        collections = self.zot.collections()
        for c in collections:
            if c["data"]["name"] == folder and (parent==None or c["data"]["parentCollection"]==False):
                return MyCollection ( self.zot, c["data"]["key"] )
        return None

    def createCollection ( self, folder ):
        x = self.zot.create_collections ( [{ "name": folder }] )
        for xi in x["successful"]:
            self.logger.info("Successfully created collection ", x["successful"][xi]["data"]["name"] )
            return MyCollection ( x["successful"][x]["key"] )
        raise Exception(x)

    def itemTypes ( self ):
        types = self.zot.item_types()
        self.logger.debug ( types )
        return types

    def itemTemplate ( self, typ, linkmode=None ):
        template = self.zot.item_template ( typ, linkmode )
        self.logger.debug ( template )
        return template

    def creatorFields ( self ):
        fields = self.zot.creator_fields()
        self.logger.debug ( fields )
        return fields

    def createItem ( self, item ):
        x = self.zot.create_items ( [item] )
        for xi in x["successful"]:
            return MyItem( self.zot, x["successful"][xi] )
        raise Exception(x)
