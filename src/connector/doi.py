import requests
from bs4 import BeautifulSoup

import re
import requests

class ContentParsingError ( Exception ):
    pass
class RequestsError ( Exception ):
    pass
class NoDoiError ( Exception ):
    pass

reflags = re.IGNORECASE | re.MULTILINE
DOI_REGEX = [
    re.compile("10.\d{4,9}\/[-._;()\/:A-Z0-9]+", reflags),
    re.compile("10.1002\/[^\s]+", reflags),
    re.compile("10.\d{4}\/\d+-\d+X?(\d+)\d+<[\d\w]+:[\d\w]*>\d+.\d+.\w+;\d", reflags),
    #re.compile("10.1021\/\w\w\d++", reflags),
    re.compile("10.1207\/[\w\d]+\&\d+_\d+", reflags),
]

def doi ( txt ):
    trust = 1.0
    for regex in DOI_REGEX:
        ret = regex.search ( txt )
        if type(ret) is re.Match:
            doi = ret.group(0)
            return ( doi, trust )

        trust -= 1/len(DOI_REGEX)

    raise NoDoiError

def guessAuthors ( entryObject ):
    pass

def _doGuessDoi ( soup, nodes="a" ):
    tags = soup.find_all ( nodes )

    possibleDoi = []
    for t in tags:
        try:
            if "href" in t:
                d = doi ( t["href"] )
                possibleDoi += d
        except NoDoiError:
            txt = t.getText()
            if len(txt) > 0:
                try:
                    d = doi ( txt )
                    possibleDoi += d
                except NoDoiError:
                    pass

    maxTrust = ("", 0.0)
    mts = []
    for d in possibleDoi:
        if d[1] > maxTrust[1]:
            maxTrust = d
            mts = [d]
        elif d[1] == maxTrust[1]:
            if maxTrust[0] != d[0]:
                mts += [d]

    if len(mts) == 0 or maxTrust[1] <= 0.1:
        raise NoDoiError

    return mts

def guessDoi ( url ):
    print ("URL:", url )
    try:
        d = doi(url)
        print ( d )
        return [d]
    except NoDoiError:
        print ( "NO DOI ERROR" )
        pass

    print ( url )
    req = requests.get( url )
    if req.status_code != 200:
        raise RequestsError ()

    content_type = req.headers.get("content-type")
    if "application/pdf" in content_type:
        pass    ## todo: download pdf and handle pdf
    elif "text/html" in content_type:
        soup = BeautifulSoup ( req.text, "html.parser" )
        # first: try all the a tags to find a DOI
        try:
            dois = _doGuessDoi ( soup, "a" )
            return dois
        except:
            NoDoiError

        # second: try a full text seach on the html code
        try:
            d = doi ( req.text )
            return [ d ]
        except:
            NoDoiError
    else:
        raise ContentParsingError

    raise NoDoiError
