#!/usr/bin/python3
import argparse
import configparser
import sys
import logging

from connector.zotero import *
from connector.pdf import *

from connector.extractor import *

from pynotifier import Notification

try:
    from connector.scihub import SciHub
    evilEnable = True
except:
    evilEnable = False

silent = False
def sendNotification ( title, descr ):
    if silent == True:
        return
    Notification(
        title="zotero-cli: %s"%title,
        description=descr,
        urgency='normal'
    ).send()

def getUserConfigDir():
    """Returns a platform-specific root directory for user config settings."""
    # On Windows, prefer %LOCALAPPDATA%, then %APPDATA%, since we can expect the
    # AppData directories to be ACLed to be visible only to the user and admin
    # users (https://stackoverflow.com/a/7617601/1179226). If neither is set,
    # return None instead of falling back to something that may be world-readable.
    if os.name == "nt":
        appdata = os.getenv("LOCALAPPDATA")
        if appdata:
            return appdata
        appdata = os.getenv("APPDATA")
        if appdata:
            return appdata
        return None
    # On non-windows, use XDG_CONFIG_HOME if set, else default to ~/.config.
    xdg_config_home = os.getenv("XDG_CONFIG_HOME")
    if xdg_config_home:
        return xdg_config_home
    return os.path.join(os.path.expanduser("~"), ".config") 

def getUserCacheDir ():
    if os.name == "nt":
        return getUserConfigDir()
    return os.path.join(os.path.expanduser("~"), ".cache")

def main ():
    parser = argparse.ArgumentParser()
    parser.add_argument("--libid", type=str, help="override the library ID from config" )
    parser.add_argument("--libtype", type=str, help="override the library type from config" )
    parser.add_argument("--apikey", type=str, help="override the api key from config" )
    parser.add_argument("--config", type=str, help="use a different config file" )
    parser.add_argument("--library", type=str, help="choose a section in the config file (defaults to \"default\")" )
    parser.add_argument("--debug", action="store_true", help="set everything to very verbose")
    parser.add_argument("--folder", type=str, help="choose a folder (collection) to place new items in" )
    parser.add_argument("--cache-dir", type=str, help="choose a directory to download PDF files to for caching" )
    parser.add_argument("--clear-cache", action="store_true", help="clear the cache directory")
    parser.add_argument("--silent", action="store_true", help="supress notifications")
    parser.add_argument("url", nargs="*", help="urls to add to zotero")
    args = parser.parse_args()

    configpath = getUserConfigDir()
    if args.config == None:
        args.config = os.path.join ( configpath, "zotero-cli" )
        os.makedirs ( args.config, exist_ok=True )
        args.config = os.path.join ( args.config, "config.ini")

    cachepath = getUserCacheDir()
    cachepath = os.path.join ( cachepath, "zotero-cli" )

    global silent
    if args.silent == True:
        silent = True;

    logLevel = logging.INFO
    if args.debug:
        logLevel = logging.DEBUG
    logging.basicConfig ( level = logLevel )

    libid = args.apikey
    libtype = args.libid
    apikey = args.libtype
    folder = args.folder
    try:
        cfg = configparser.ConfigParser()
        cfg.read( args.config )
        if args.library != None:
            cfg = cfg[ args.library ]
        else:
            cfg = cfg[ "default" ]

        evilMode = (evilEnable == True and cfg.getboolean("evil") == True)
        apikey = args.apikey or cfg.get("apikey" )
        libid = args.libid or cfg.get ( "libid")
        libtype = args.libtype or cfg.get ( "libtype" )
        folder = args.folder or cfg.get ( "folder" )
    except:
        logging.warning("Was not able to read the config file")
        evilMode = False

    if libid == None or libtype == None or apikey == None:
        return

    if folder == None:
        folder = "INBOX"

    logger = logging.getLogger("zotero")
    logger.setLevel ( logLevel )
    crlogger = logging.getLogger("crossref")
    crlogger.setLevel ( logLevel )
    #try:
    zot = MyZotero ( libid, libtype, apikey, logger, cachepath )
    cr = MyCrossRef ( zot, logger=crlogger )
    if evilMode:
        sclogger = logging.getLogger("sci-hub")
        sclogger.setLevel ( logLevel )
        sh = SciHub(sclogger)
    else:
        sh = None

    zoteroInserter = ZoteroInserter ( zot, cr, sh, collection=folder )
    for u in args.url:
        StartExtraction ( u, zoteroInserter )
    return;
    #except:
    #    logging.critical ("Unable to connect to Zotero")
    #    return

    ## Create the Zotero Collection if it does not exist
    col = zot.getCollection ( folder )
    if col == None:
        col = zot.createCollection ( folder )

    retval = []
    for u in args.url:
        pdffile = ""
        if os.path.isfile(u):
            xmp = discoverMetadata ( u )
            if xmp.doi == None:
                logger.critical ( "Could not determine a DOI, found in metadata: %s", xmp )
                continue
            pdffile = u
            u = xmp.doi


        try:
            if col.present ( u ):
                logging.critical ("URL already in folder: %s", folder )
                raise AlreadyPresentError( u )
        except AlreadyPresentError as e:
            sendNotification ("already present", e.err )
            continue

        try:
            itemList = cr.query ( u )
        except QueryError as e:
            sendNotification ( "failed query", e.err )
            continue
        except QueryParseError as e:
            sendNotification ("failed crossref parse", e.err )
            continue

        for item in itemList:
            (t, urls, _) = item
            if t["extra"] == "":
                t["extra"] = u

            ret = zot.createItem ( t )
            col.addTo ( ret )

            retval += [ t["title"]]

            if len(pdffile) > 0:
                ret.addAttachment ( pdffile )
            else:
                try:
                    if len(urls) > 0:
                        ret.transferPDF ( urls )
                except DownloadError as e:
                    try:
                        if evilMode == True:
                            if t["DOI"] != "":
                                ret.transferEvilPDF ( sh, t["DOI"] )
                            else:
                                ret.transferEvilPDF ( sh, u )
                        else:
                            raise e
                    except DownloadError as ex:
                        sendNotification ( "failed download", ex.err )

    msg = ""
    for r in retval:
        msg += " - %s\n"%r
    if msg != "":
        sendNotification ( "Successful", msg )
    else:
        sendNotification ( "Import Failed", "" )

main()
