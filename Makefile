package:
	python -m build

install:
	make package
	pip install .

uninstall:
	pip uninstall zotero-cli

test:
	cd tests &&\
		python -m unittest discover . "*_test.py"
