# zotero-cli

CLI-Connector for zotero, can be used to insert entries into your Zotero library for example from inside qutebrowser or by shell. Tries to attach the PDF if it can be downloaded.

## Installation

```
git clone https://codeberg.org/snaums/zotero-cli.git
cd zotero-cli

make install
# or 
python -m build
pip install .
```

## Configuration

Zotero uses a configuration file in `~/.config/zotero-cli/config.ini`. It looks much like the example-file. You'll need an API-key and the ID of the zotero library, as well as the type of the library (`user` or `group`).

Log into your Zotero-Account, then
* [Create an API-Key](https://www.zotero.org/settings/keys). There you'll also find your **personal** library-ID (use libtype `user`)
* [Groups](https://www.zotero.org/groups/), then hover over the group-name, `https://www.zotero.org/groups/<libID>/<name>`. Then the libtype is `group`.

```
[default]
apikey=
folder=CLI_INBOX
libid=
libtype=user
```

## Running

When the configuration is set up correctly, zotero-cli can be used by just passing an URL or path to a PDF (if there is metadata embedded).

```
zotero-cli.py [url]
```

zotero-cli takes the following parameters:

```
usage: zotero-cli.py [-h] [--libid LIBID] [--libtype LIBTYPE] [--apikey APIKEY] [--config CONFIG]
                     [--library LIBRARY] [--debug] [--folder FOLDER] [--cache-dir CACHE_DIR] [--clear-cache]
                     [--silent]
                     [url ...]

positional arguments:
  url                   urls to add to zotero

optional arguments:
  -h, --help            show this help message and exit
  --libid LIBID         override the library ID from config
  --libtype LIBTYPE     override the library type from config
  --apikey APIKEY       override the api key from config
  --config CONFIG       use a different config file
  --library LIBRARY     choose a section in the config file (defaults to "default")
  --debug               set everything to very verbose
  --folder FOLDER       choose a folder (collection) to place new items in
  --cache-dir CACHE_DIR
                        choose a directory to download PDF files to for caching
  --clear-cache         clear the cache directory
  --silent              supress notifications
```

### qutebrowser

Zotero-cli was meant for the qutebrowser webbrowser. If you'd also like to use it inside the qutebrowser, it's configuration could look something like this (`~/.config/qutebrowser/autoconfig.yml`):

```
settings:
  bindings.commands:
    global:
      normal:
        ',Z': hint links spawn zotero-cli.py {hint-url}
        ',z': spawn zotero-cli.py {url}
```

Pressing `,z` will try to insert the current URL into your zotero-library, `,Z` will show link-hints. After selecting one of them, the selected one will be inserted.

## Development and Bugs

If you find any bugs or libraries, which are not currently supported, please report them as an issue.

Starting development, you can have a look at the `connector/extractor.py`. An Extractor is a piece of code, which starts from an input (most likely an URL or DOI) and creates a zotero-entry. E.g. the usenix-Extractor gets the website, finds the PDF link and extracts the metadata for the zotero-entry from the meta-tags in the HTML-code.

Adding an extractor is easy, first try to find out if the input is suitable for your extractor (`isSupported`), then extract an entry. Raise a `CannotHandle` exception if extraction does not work out. Please also add a few tests for your extractor (or the code behind it).
