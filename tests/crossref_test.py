import unittest

import json
import sys
sys.path.append("../src")
from connector import zotero
from connector.cr import MyCrossRef

from datetime import date

class CrossRefTest ( unittest.TestCase ):
    def setUp ( self ):
        self.maxDiff = None

    def test_parseConferencePaper ( self ):
        cr = ""
        with open("../tests/facebook_input.json", "r") as f:
            cr = json.loads(f.read())

        tmpl = {'itemType': 'conferencePaper', 'title': '', 'creators': [{'creatorType': 'author', 'firstName': '', 'lastName': ''}], 'abstractNote': '', 'date': '', 'proceedingsTitle': '', 'conferenceName': '', 'place': '', 'publisher': '', 'volume': '', 'pages': '', 'series': '', 'language': '', 'DOI': '', 'ISBN': '', 'shortTitle': '', 'url': '', 'accessDate': '', 'archive': '', 'archiveLocation': '', 'libraryCatalog': '', 'callNumber': '', 'rights': '', 'extra': '', 'tags': [], 'collections': [], 'relations': {}}

        mycr = MyCrossRef ( None )
        ret = mycr.itemTransfer ( tmpl, cr )

        with open("../tests/facebook_output.json", "r") as f:
            crout = json.loads(f.read())
        crout["accessDate"] = date.today().isoformat()
        self.assertDictEqual ( tmpl, crout )

    def test_findPDF ( self ):
        cr = ""
        with open("../tests/facebook_input.json", "r") as f:
            cr = json.loads(f.read())

        mycr = MyCrossRef ( None )
        urls = mycr.getPdfUrls ( None, cr )

        #self.assertEqual ( len(urls), 2 )
        for u in urls:
            self.assertEqual ( u, "https://dl.acm.org/doi/pdf/10.1145/3190508.3190524" )


