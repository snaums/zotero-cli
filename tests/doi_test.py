import unittest

import sys
sys.path.append("../src")
from connector import doi

class DoiTest ( unittest.TestCase ):
    def testDoiGuessing ( self ):
        tests = [
            ("https://dl.acm.org/doi/10.1145/3302424.3303946","10.1145/3302424.3303946"),
            ("https://ieeexplore.ieee.org/document/8525022","10.1109/ITMQIS.2018.8525022"),
            ("https://link.springer.com/article/10.1007/s11192-015-1547-0", "10.1007/s11192-015-1547-0"),
        ]
        for u in tests:
            with self.subTest(url=u[0]):
                d = doi.guessDoi ( u[0] )
                self.assertEqual(len(d), 1)
                self.assertEqual(d[0][0], u[1])

