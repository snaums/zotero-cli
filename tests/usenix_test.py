import unittest

import sys
sys.path.append("../src")
from connector import usenix
from connector import extractor

class UsenixTest ( unittest.TestCase ):
    template = {'itemType': 'conferencePaper', 'title': '', 'creators': [{'creatorType': 'author', 'firstName': '', 'lastName': ''}], 'abstractNote': '', 'date': '', 'proceedingsTitle': '', 'conferenceName': '', 'place': '', 'publisher': '', 'volume': '', 'pages': '', 'series': '', 'language': '', 'DOI': '', 'ISBN': '', 'shortTitle': '', 'url': '', 'accessDate': '', 'archive': '', 'archiveLocation': '', 'libraryCatalog': '', 'callNumber': '', 'rights': '', 'extra': '', 'tags': [], 'collections': [], 'relations': {}}
    comparison = {'itemType': 'conferencePaper', 'title': 'MAGE: Nearly Zero-Cost Virtual Memory for Secure Computation', 'creators': [{'creatorType': 'author', 'firstName': 'Sam', 'lastName': 'Kumar'}, {'creatorType': 'author', 'firstName': 'David E.', 'lastName': 'Culler'}, {'creatorType': 'author', 'firstName': 'Raluca Ada', 'lastName': 'Popa'}], 'abstractNote': '', 'date': '2021', 'proceedingsTitle': '15th USENIX Symposium on Operating Systems Design and Implementation (OSDI 21)', 'conferenceName': '15th USENIX Symposium on Operating Systems Design and Implementation (OSDI 21)', 'place': '', 'publisher': 'Usenix', 'volume': '', 'pages': '', 'series': '', 'language': '', 'DOI': '', 'ISBN': '', 'shortTitle': '', 'url': '', 'accessDate': 'CURRENT_TIMESTAMP', 'archive': '', 'archiveLocation': '', 'libraryCatalog': '', 'callNumber': '', 'rights': '', 'extra': '367 385', 'tags': [], 'collections': [], 'relations': {}, 'isbn': '978-1-939133-22-9'}
    tests = [ "https://www.usenix.org/conference/osdi21/presentation/kumar"]

    def testProbe ( self ):
        for u in UsenixTest.tests:
            with self.subTest(url=u):
                self.assertEqual(usenix.UsenixMeta.Probe(u), True)

    def testExtract ( self ):
        for u in UsenixTest.tests:
            with self.subTest(url=u):
                extracted = extractor.Item(u)
                x = usenix.UsenixMeta.Extract ( extracted, template=UsenixTest.template )
                self.assertEqual(x, "finished")
                self.assertEqual(extracted.zotero, UsenixTest.comparison)

if __name__ == '__main__':
    unittest.main()
