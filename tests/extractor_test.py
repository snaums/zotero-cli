import unittest

import os
import sys
sys.path.append("../src")
from connector import extractor
import configparser
import logging
from connector import zotero
from connector import cr

from datetime import date

class ExtractorTest ( unittest.TestCase ):
    template = {'itemType': 'conferencePaper', 'title': '', 'creators': [{'creatorType': 'author', 'firstName': '', 'lastName': ''}], 'abstractNote': '', 'date': '', 'proceedingsTitle': '', 'conferenceName': '', 'place': '', 'publisher': '', 'volume': '', 'pages': '', 'series': '', 'language': '', 'DOI': '', 'ISBN': '', 'shortTitle': '', 'url': '', 'accessDate': '', 'archive': '', 'archiveLocation': '', 'libraryCatalog': '', 'callNumber': '', 'rights': '', 'extra': '', 'tags': [], 'collections': [], 'relations': {}}
    comparison = [{'itemType': 'conferencePaper', 'title': 'MAGE: Nearly Zero-Cost Virtual Memory for Secure Computation', 'creators': [{'creatorType': 'author', 'firstName': 'Sam', 'lastName': 'Kumar'}, {'creatorType': 'author', 'firstName': 'David E.', 'lastName': 'Culler'}, {'creatorType': 'author', 'firstName': 'Raluca Ada', 'lastName': 'Popa'}], 'abstractNote': '', 'date': '2021', 'proceedingsTitle': '15th USENIX Symposium on Operating Systems Design and Implementation (OSDI 21)', 'conferenceName': '15th USENIX Symposium on Operating Systems Design and Implementation (OSDI 21)', 'place': '', 'publisher': 'Usenix', 'volume': '', 'pages': '', 'series': '', 'language': '', 'DOI': '', 'ISBN': '', 'shortTitle': '', 'url': '', 'accessDate': 'CURRENT_TIMESTAMP', 'archive': '', 'archiveLocation': '', 'libraryCatalog': '', 'callNumber': '', 'rights': '', 'extra': '367 385', 'tags': [], 'collections': [], 'relations': {}, 'isbn': '978-1-939133-22-9'}, {'itemType': 'conferencePaper', 'title': 'RT.js: Practical Real-Time Scheduling for Web Applications', 'creators': [{'creatorType': 'author', 'firstName': 'Christian', 'lastName': 'Dietrich'}, {'creatorType': 'author', 'firstName': 'Stefan', 'lastName': 'Naumann'}, {'creatorType': 'author', 'firstName': 'Robin', 'lastName': 'Thrift'}, {'creatorType': 'author', 'firstName': 'Daniel', 'lastName': 'Lohmann'}], 'abstractNote': '', 'date': '2020-09-23', 'proceedingsTitle': '2019 IEEE Real-Time Systems Symposium (RTSS)', 'conferenceName': '2019 IEEE Real-Time Systems Symposium (RTSS)', 'place': 'Hong Kong, Hong Kong', 'publisher': 'IEEE', 'volume': '', 'pages': '', 'series': '', 'language': '', 'DOI': '10.1109/rtss46320.2019.00017', 'ISBN': '', 'shortTitle': '', 'url': 'http://xplorestaging.ieee.org/ielx7/9040680/9052112/09052116.pdf?arnumber=9052116', 'accessDate': date.today().isoformat(), 'archive': '', 'archiveLocation': '', 'libraryCatalog': '', 'callNumber': '', 'rights': '', 'extra': '', 'tags': [], 'collections': [], 'relations': {}}, {'itemType': 'conferencePaper', 'title': 'The open access advantage considering citation, article usage and social media attention', 'creators': [{'creatorType': 'author', 'firstName': 'Xianwen', 'lastName': 'Wang'}, {'creatorType': 'author', 'firstName': 'Chen', 'lastName': 'Liu'}, {'creatorType': 'author', 'firstName': 'Wenli', 'lastName': 'Mao'}, {'creatorType': 'author', 'firstName': 'Zhichao', 'lastName': 'Fang'}], 'abstractNote': '', 'date': '2015-03-12', 'proceedingsTitle': 'Scientometrics', 'conferenceName': '', 'place': '', 'publisher': 'Springer Science and Business Media LLC', 'volume': '', 'pages': '', 'series': '', 'language': '', 'DOI': '10.1007/s11192-015-1547-0', 'ISBN': '', 'shortTitle': '', 'url': 'http://link.springer.com/content/pdf/10.1007/s11192-015-1547-0.pdf', 'accessDate': date.today().isoformat(), 'archive': '', 'archiveLocation': '', 'libraryCatalog': '', 'callNumber': '', 'rights': '', 'extra': '', 'tags': [], 'collections': [], 'relations': {}}, {'itemType': 'conferencePaper', 'title': 'SkyBridge: Fast and Secure Inter-Process Communication for Microkernels', 'creators': [{'creatorType': 'author', 'firstName': 'Zeyu', 'lastName': 'Mi'}, {'creatorType': 'author', 'firstName': 'Dingji', 'lastName': 'Li'}, {'creatorType': 'author', 'firstName': 'Zihan', 'lastName': 'Yang'}, {'creatorType': 'author', 'firstName': 'Xinran', 'lastName': 'Wang'}, {'creatorType': 'author', 'firstName': 'Haibo', 'lastName': 'Chen'}], 'abstractNote': '', 'date': '2019-03-25', 'proceedingsTitle': 'Proceedings of the Fourteenth EuroSys Conference 2019', 'conferenceName': "EuroSys '19: Fourteenth EuroSys Conference 2019", 'place': 'Dresden Germany', 'publisher': 'ACM', 'volume': '', 'pages': '', 'series': '', 'language': '', 'DOI': '10.1145/3302424.3303946', 'ISBN': '', 'shortTitle': '', 'url': 'https://dl.acm.org/doi/pdf/10.1145/3302424.3303946', 'accessDate': date.today().isoformat(), 'archive': '', 'archiveLocation': '', 'libraryCatalog': '', 'callNumber': '', 'rights': '', 'extra': '', 'tags': [], 'collections': [], 'relations': {}}, {'itemType': 'conferencePaper', 'title': 'HEAPO: Heap-Based Persistent Object Store', 'creators': [{'creatorType': 'author', 'firstName': 'Taeho', 'lastName': 'Hwang'}, {'creatorType': 'author', 'firstName': 'Jaemin', 'lastName': 'Jung'}, {'creatorType': 'author', 'firstName': 'Youjip', 'lastName': 'Won'}], 'abstractNote': '', 'date': '2015-02-24', 'proceedingsTitle': 'ACM Transactions on Storage', 'conferenceName': '', 'place': '', 'publisher': 'Association for Computing Machinery (ACM)', 'volume': '', 'pages': '', 'series': '', 'language': '', 'DOI': '10.1145/2629619', 'ISBN': '', 'shortTitle': '', 'url': 'https://dl.acm.org/doi/pdf/10.1145/2629619', 'accessDate': date.today().isoformat(), 'archive': '', 'archiveLocation': '', 'libraryCatalog': '', 'callNumber': '', 'rights': '', 'extra': '', 'tags': [], 'collections': [], 'relations': {}}, {'itemType': 'conferencePaper', 'title': 'HEAPO: Heap-Based Persistent Object Store', 'creators': [{'creatorType': 'author', 'firstName': 'Taeho', 'lastName': 'Hwang'}, {'creatorType': 'author', 'firstName': 'Jaemin', 'lastName': 'Jung'}, {'creatorType': 'author', 'firstName': 'Youjip', 'lastName': 'Won'}], 'abstractNote': '', 'date': '2015-02-24', 'proceedingsTitle': 'ACM Transactions on Storage', 'conferenceName': '', 'place': '', 'publisher': 'Association for Computing Machinery (ACM)', 'volume': '', 'pages': '', 'series': '', 'language': '', 'DOI': '10.1145/2629619', 'ISBN': '', 'shortTitle': '', 'url': 'https://dl.acm.org/doi/pdf/10.1145/2629619', 'accessDate': date.today().isoformat(), 'archive': '', 'archiveLocation': '', 'libraryCatalog': '', 'callNumber': '', 'rights': '', 'extra': '', 'tags': [], 'collections': [], 'relations': {}}, {'itemType': 'conferencePaper', 'title': 'Stroke Parameterization', 'creators': [{'creatorType': 'author', 'firstName': 'R.', 'lastName': 'Schmidt'}], 'abstractNote': '', 'date': '2021-07-04', 'proceedingsTitle': 'Computer Graphics Forum', 'conferenceName': '', 'place': '', 'publisher': 'Wiley', 'volume': '', 'pages': '', 'series': '', 'language': '', 'DOI': '10.1111/cgf.12045', 'ISBN': '', 'shortTitle': '', 'url': 'https://api.wiley.com/onlinelibrary/tdm/v1/articles/10.1111%2Fcgf.12045', 'accessDate': date.today().isoformat(), 'archive': '', 'archiveLocation': '', 'libraryCatalog': '', 'callNumber': '', 'rights': '', 'extra': '', 'tags': [], 'collections': [], 'relations': {}}, {'itemType': 'conferencePaper', 'title': 'Stroke Parameterization', 'creators': [{'creatorType': 'author', 'firstName': 'R.', 'lastName': 'Schmidt'}], 'abstractNote': '', 'date': '2021-07-04', 'proceedingsTitle': 'Computer Graphics Forum', 'conferenceName': '', 'place': '', 'publisher': 'Wiley', 'volume': '', 'pages': '', 'series': '', 'language': '', 'DOI': '10.1111/cgf.12045', 'ISBN': '', 'shortTitle': '', 'url': 'https://api.wiley.com/onlinelibrary/tdm/v1/articles/10.1111%2Fcgf.12045', 'accessDate': date.today().isoformat(), 'archive': '', 'archiveLocation': '', 'libraryCatalog': '', 'callNumber': '', 'rights': '', 'extra': '', 'tags': [], 'collections': [], 'relations': {}}]
    tests = [
        "https://www.usenix.org/conference/osdi21/presentation/kumar",
        "https://ieeexplore.ieee.org/document/9052116",
        "https://link.springer.com/article/10.1007/s11192-015-1547-0",
        "https://dl.acm.org/doi/10.1145/3302424.3303946",
        "https://doi.org/10.1145/2629619",
        "10.1145/2629619",
        "https://doi.org/10.1111/cgf.12045",
        "10.1111/cgf.12045",
    ]

    def setUp ( self ):
        self.maxDiff = None
        config = os.path.join(os.path.expanduser("~"), ".config")
        config = os.path.join ( config, "zotero-cli" )
        os.makedirs ( config, exist_ok=True )
        config = os.path.join ( config, "config.ini")
        cfg = configparser.ConfigParser()
        cfg.read( config )
        cfg = cfg[ "default" ]

        apikey = cfg.get("apikey" )
        libid = cfg.get ( "libid")
        libtype = cfg.get ( "libtype" )
        folder = "automatic-tests"

        logger = logging.getLogger("zotero")
        logger.setLevel ( logging.FATAL )
        crlogger = logging.getLogger("crossref")
        crlogger.setLevel ( logging.FATAL )

        zot = zotero.MyZotero ( libid, libtype, apikey, logger, "" )
        cross = cr.MyCrossRef ( zot, logger=crlogger )
        self.inserter = extractor.TestInserter( zot, cross )

    def testExtract ( self ):
        tests = ExtractorTest.tests
        comparison = ExtractorTest.comparison
        for i in range(len(tests)):
            with self.subTest(url=tests[i]):
                item = extractor.StartExtraction ( tests[i], self.inserter, template=ExtractorTest.template )
                self.assertEqual(item.zotero, comparison[i])


if __name__ == '__main__':
    unittest.main()
